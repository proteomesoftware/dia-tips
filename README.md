# README #

Data Independent Acquisition (DIA) Tips.
Currently hosted on https://proteomesoftware.bitbucket.io/dia-tips/

### What is this repository for? ###

* Use this repo to collect and store helpful tips for getting started with DIA proteomics

### How do I get set up? ###

* Clone and push `index.html` to a webserver

### Contribution guidelines ###

* Feel free to submit pull request (PR) or fork for your own use

### Who do I talk to? ###

* admin@proteomesoftware.com